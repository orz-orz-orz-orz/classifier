import pandas

trn_tsv = pandas.read_csv("cv-corpus-7.0-2021-07-21/zh-TW/train.tsv", sep='\t')
filtered = trn_tsv[ (trn_tsv['accent'].notnull()) ]
filtered.to_csv('train.csv')
print(trn_tsv['accent'].unique())
print(filtered['accent'].unique())
print(trn_tsv.shape, filtered.shape)
