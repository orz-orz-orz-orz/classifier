#!/usr/bin/env python
import logging
import pathlib

import toml
import torch
import torchaudio
import pandas
import torch.nn as nn
import torch.nn.functional as F

from torch.utils.data import DataLoader
from torch.optim import AdamW

# some modules for DDP
import os
import itertools
import torch.multiprocessing as mp
import torch.distributed as distributed
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data.distributed import DistributedSampler as DS
from torch.utils.data import RandomSampler as RS
from torch.utils.data import SequentialSampler as SS
from torchaudio.transforms import MelSpectrogram, Resample

from argparse import ArgumentParser

from torch import Tensor
from typing import Union, List



ACCENTS=['unknown',
         'keelung_city', 'taipei_city', 'new_taipei_city', 
         'taoyuan_city', 'hsinchu_county', 'hsinchu_city', 'miaoli_county', 
         'taichung_city', 'changhua_county', 'nantou_county', 
         'yunlin_county', 'chiayi_county', 'chiayi_city', 'tainan_city', 
         'kaohsiung_city', 'pingtung_county', 
         'yilan_county', 'hualien_county', 'taitung_county',
         'kinmen_county', 'hong_kong', 'other'
        ]

NORTH = ['keelung_city', 'taipei_city', 'new_taipei_city', 
         'taoyuan_city', 'hsinchu_county', 'hsinchu_city']
CENTRAL = ['miaoli_county', 'taichung_city', 'changhua_county', 
        'nantou_county', 'yunlin_county']
SOUTH = ['chiayi_county', 'chiayi_city', 'tainan_city', 
         'kaohsiung_city', 'pingtung_county']
EAST = ['yilan_county', 'hualien_county', 'taitung_county']

OTHER = ['kinmen_county', 'hong_kong', 'other' ]


def accent_to_index(accent):
    try:
        index = ACCENTS.index(accent)
    except ValueError:
        index = 0
    return index        

def index_to_accent(index):
    return ACCENTS[index]

def accent_to_area_index(accent):
    if accent in NORTH:
        return 1
    if accent in CENTRAL:
        return 2
    if accent in SOUTH:
        return 3
    if accent in EAST:
        return 4
    if accent in OTHER:
        return 5
    else:
        return 0


class CSVDataset(torch.utils.data.Dataset):
    def __init__(self, csv_path, root, sr=None, n_fft=2048, hop_length=256, n_mels=128):
        super().__init__()
        self.csv = pandas.read_csv(csv_path)
        self.root = pathlib.Path(root)
        self.sr = sr
        self.ms = MelSpectrogram(sample_rate=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels)
    
    def __len__(self):
        return len(self.csv)
    
    def __getitem__(self, idx):
        filename = self.csv['path'][idx]
        wav, sr = torchaudio.load(self.root/filename)
        if self.sr is not None and sr != self.sr:
            resample = Resample(sr, self.sr)  # resample only accepts (C,T) format
            wav = resample(wav)
        
        wav = wav[0] # only preserve left channel   
        
        sp = self.ms.forward(wav)
        
        label = self.csv['accent'][idx]
        label = accent_to_area_index(label)
        return sp, label

class Classifier(nn.Module):
    def __init__(self, in_channels, out_channels, h_channels, n_layers):
        super().__init__()
        self.rnn = nn.GRU(in_channels, h_channels, n_layers, batch_first=True) 
        self.lin = nn.Linear(h_channels, out_channels)
    
    def forward(self, x, q=None):
        x = x.transpose(1, 2) # BCT -> BTC
        h, _ = self.rnn(x) # BTH
        # select the last frame
        if q is not None:
            idx = (q-1).view(-1, 1, 1).expand(-1, 1, h.shape[2])
            h = h.gather(1, idx).squeeze(1)
        else:
            h = h[:, -1, :]
        h = self.lin(h)# BO
        h = F.log_softmax(h, dim=1)
        return h

def collate_fn(batch):
    wav_list = []
    length_list = []
    label_list = []
    for wav, label in batch:
        wav_list.append(wav)
        length_list.append(wav.shape[-1])
        label_list.append(label)
    
    max_length = max(length_list)
    w = torch.stack([F.pad(x, (0, max_length - x.shape[-1])) for x in wav_list])
    l = torch.tensor(label_list, dtype=torch.long)
    q = torch.tensor(length_list, dtype=torch.long)
    return w, l, q


def config_logger(log_path, name=''):
    logger = logging.getLogger(str(name))
    logger_formatter = logging.Formatter('[%(levelname)s] %(asctime)s %(message)s', datefmt='%m-%d %H:%M:%S')
    logger_handlers = [logging.FileHandler(log_path, mode='w'),
                       logging.StreamHandler()]
    for h in logger_handlers:
        h.setFormatter(logger_formatter)
        logger.addHandler(h)
    logger.setLevel(logging.INFO)
    logger.propagate = False
    return logger


def ddp_setup(rank, world_size):
    # check the gpu
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12345'
    if torch.cuda.device_count() > 0:
        distributed.init_process_group("nccl", rank=rank, world_size=world_size)
    else:
        distributed.init_process_group("gloo", rank=rank, world_size=world_size)

def ddp_cleanup():
    distributed.destroy_process_group()


pretrained_model_path = None


def tuple2d(list2d):
    return tuple( map(tuple, list2d) )

def build_model(rank, cfg):
    logging.info("Make The Model.")
    if pretrained_model_path is not None:
        model = torch.load(pretrained_model_path)
        logging.info(f"load model from {pretrained_model_path}")
        return model
    
    model = Classifier(cfg['n_mels'], cfg['n_classes'], cfg['h_channels'], cfg['n_layers'])

    return model


def build_dataloader(csv_path, data_root, sample_rate, n_fft, hop_length, n_mels,
                     batch_size, shuffle, num_workers, drop_last, distributed=True):
    data = CSVDataset(csv_path, data_root, sample_rate, n_fft, hop_length, n_mels)
    if distributed:
        sampler = DS(data, shuffle=shuffle, drop_last=True) # 這邊 drop_last 是用來分割資料集的
    else:
        if shuffle:
            sampler = RS(data)
        else:
            sampler = SS(data)

    data_loader = DataLoader(data, batch_size=batch_size,
                             num_workers=num_workers, pin_memory=True,
                             sampler=sampler, drop_last=drop_last, collate_fn=collate_fn)
    return sampler, data_loader





def get_dataloaders(config):
    # ['err'] + ['lpc', 'src'] or ['wav'] + ['mgc', 'src']

    trn_sa, trn_dl = build_dataloader(config['trn_csv_path'], config['data_root'], config['sample_rate'], 
                                      config['n_fft'], config['hop_length'], config['n_mels'], config['batch_size'],
                                      shuffle=True, num_workers=5, drop_last=True, distributed=True)

    val_sa, val_dl = build_dataloader(config['val_csv_path'], config['data_root'], config['sample_rate'], 
                                      config['n_fft'], config['hop_length'], config['n_mels'], config['batch_size'],
                                      shuffle=False, num_workers=5, drop_last=True, distributed=True)
    return trn_sa, trn_dl, val_sa, val_dl


def save(model, state, save_dir, suffix=''):
    save_dir = pathlib.Path(save_dir)
    save_dir.mkdir(parents=True, exist_ok=True)

    if isinstance(model, DDP):
        model = model.module

    if suffix != '':
        suffix = f"_{suffix}"

    torch.save(model, save_dir / f'model{suffix}.pt')
    if state:
        torch.save(state, save_dir / f'state{suffix}.pt')


def train(rank, world_size, log_path, config):
    ddp_setup(rank, world_size)

    logger = config_logger(log_path, rank)
    logger.info(f"start training from node {rank}")

    # get the current device
    device = torch.device(rank if torch.cuda.is_available() else 'cpu')

    torch.cuda.set_device(rank)
    torch.cuda.empty_cache()

    # builde the model
    model = build_model(rank, config)

    # transfer model to device
    model = model.to(device)
    # wrap the model in ddp
    if world_size > 1:
        model = DDP(model, device_ids=[rank], find_unused_parameters=True)

    # get the optimizer
    optimizer = AdamW(model.parameters(), config['lr'])

    # get the dataloader
    trn_sa, trn_dl, val_sa, val_dl = get_dataloaders(config)

    running_loss = None
    avg_loss = None
    min_loss = None
    step = 0
    epoch = 0
    max_trn_batch = len(trn_dl)
    max_val_batch = len(val_dl)

    try:
        for e in range(config['max_epochs']):
            model.train()
            # set the epoch to correctly shuffle the data
            if world_size > 1:
                trn_sa.set_epoch(e)
                val_sa.set_epoch(e)

            for i, batch in enumerate(trn_dl):
                w, l, q = (o.to(device) for o in batch)

                # train the discriminator
                optimizer.zero_grad()

                p = model.forward(w, q)
                loss = F.nll_loss(p, l)

                loss.backward()
                optimizer.step()

                step += 1
                running_loss = running_loss / 2.0 + loss.item() / 2.0 if running_loss else loss.item()
                if rank == 0:
                    if step % 5 == 0:
                        logger.info(f"[train] step:{step}, "
                                    f"running_loss={running_loss:.6f}, "
                                    f"loss ={loss.item():.6f}, "
                                    )

            if rank == 0:
                logger.info(f"epoch: {epoch}, step:{step}, running_loss={running_loss:.6f}")
                state = {"epoch": epoch, "step": step, "avg_loss": avg_loss}
                save(model, state, config['save_dir'], suffix='last')
                logger.info("save >> model_last.pt")

            # distributed.barrier()
            # 只有第 0 個 process 要監控 validation set
            avg_loss = 0.0

            model.eval()

            with torch.no_grad():
                for i, batch in enumerate(val_dl):
                    w, l, q = (o.to(device) for o in batch)
                    p = model.forward(w, q)
                    loss = F.nll_loss(p, l)

                    # 可以使用 torch.distributed.all_reduce 同步每個 process 的 tensor
                    if world_size > 1:
                        distributed.all_reduce(loss, async_op=True)

                    avg_loss += loss.item() / max_val_batch

                    if i % 10 == 0:
                        if rank == 0:
                            logger.info(f"[eval] step:{step}, "
                                        f"loss={loss.item():.6f}")
                if rank == 0:
                    logger.info(
                        f"[eval] epoch:{epoch}, step:{step}, avg_loss={avg_loss:.6f}")

            if min_loss is None or min_loss > avg_loss:
                if rank == 0:
                    min_loss = avg_loss
                    state = {"epoch": epoch, "step": step, "avg_loss": avg_loss}
                    save(model, state, config['save_dir'], suffix='best')
                    logger.info("save >> model_best.pt")
            # distributed.barrier()

            epoch += 1
            # scheduler.step(None)
        ddp_cleanup()
        # end{for e in range(config.max_epochs)}
    except KeyboardInterrupt:
        if rank == 0:
            state = {"epoch": epoch, "step": step, "avg_loss": avg_loss}
            save(model, state, config['save_dir'], suffix='break')
            logger.info("catch KeyboardInterrupt. save >> model_break.pt.")
        ddp_cleanup()


def main():
    parser = ArgumentParser()
    parser.add_argument("-c", "--config", type=str, default='configs/config.toml', help="the configs file")
    parser.add_argument("-l", "--log", type=str, default="logs/f_train_hifigan_ddp.log", help="path to the log file")

    options = parser.parse_args()

    config = toml.load(options.config)

    log_path = pathlib.Path(options.log)
    log_path.parent.mkdir(parents=True, exist_ok=True)

    logger = config_logger(log_path)

    logger.info("#########################################")
    logger.info("#  g:     Train DiffWave Model(DDP)     #")
    logger.info("#########################################")


    if torch.cuda.device_count() > 0:
        world_size = torch.cuda.device_count()
        mp.spawn(train,
                 args=(world_size, log_path, config),
                 nprocs=world_size,
                 join=True)
    else:
        train(1, 1, log_path, config)


# only the main process will execute this part.
if __name__ == "__main__":
    main()
